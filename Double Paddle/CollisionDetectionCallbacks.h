#pragma once

#include <Box2D/Box2D.h>
#include <list>
#include "PhysicsEntity.h"

class PhysicsEntity;

typedef Command<b2Contact* /*contact*/, PhysicsEntity* /*A*/, PhysicsEntity* /*B*/> EndContactCallback;
typedef Command<b2Contact* /*contact*/, const b2ContactImpulse* /*impulse*/, PhysicsEntity* /*A*/, PhysicsEntity* /*B*/> PostSolveCallback;

class CollisionDetectionCallbacks : public b2ContactListener
{
public:
	virtual void BeginContact(b2Contact* contact);
	virtual void EndContact(b2Contact* contact);
	virtual void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
	virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);

	void AddEndContactCallback(EndContactCallback callback);
	void RemoveEndContactCallback(EndContactCallback callback);
	void AddPostSolveCallback(PostSolveCallback callback);
	void RemovePostSolveCallback(PostSolveCallback callback);

private:
	std::list<EndContactCallback> endContactCallbacks;
	std::list<PostSolveCallback> postSolveCallbacks;
};