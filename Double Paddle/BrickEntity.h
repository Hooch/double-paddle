#pragma once
#include "PhysicsEntity.h"
#include "DrawableEntity.h"
#include "GlobalDefines.h"
#include <algorithm>

#include <SFML/Graphics.hpp>

class BrickEntity : public PhysicsEntity, public DrawableEntity
{
public:
	BrickEntity(int health = 100, int maxHealth = 100);
	BrickEntity(b2World& world);

	//Physics
	virtual EntityType GetEntityType() { return EntityType::ET_BRICK; }
	virtual void PostSolve(PhysicsEntity& other, b2Contact* contact, const b2ContactImpulse* impulse);
	virtual void EndContact(PhysicsEntity& other, b2Contact* contact);
	virtual void Update(const sf::Time& currentTime, const sf::Time& dTime);
	
	//Drawable
	virtual void Draw(sf::RenderWindow& renderWnd);

	void Register(b2World& world, const b2Vec2& initialPosition);

	void SetHealth(int health);
	int GetHealth();
	void SetMaxHealth(int maxHealth);
	int GetMaxHealth();

	sf::Vector2f GetSize();
	
private:
	b2BodyDef CreateBodyDefinition(const b2Vec2& initialPosition);

	int health;
	int maxHealth;
	sf::Vector2f size;

	sf::RectangleShape shape;
};