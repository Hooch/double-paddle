#include "StartScreenScene.h"

StartScreenScene::StartScreenScene() :
projectionView({ 0, 0, 1600, 1200 }), background(projectionView.getSize())
{
	isLoaded = false;
}

StartScreenScene::~StartScreenScene()
{
	
}

void StartScreenScene::Load()
{
	titleFont.loadFromFile("Assets\\Fonts\\JLSSpaceGothicR_NC.otf");

	titleText.setFont(titleFont);
	titleText.setColor(sf::Color::White);
	titleText.setCharacterSize(250);
	titleText.setPosition(50, 10);

	bottomText.setFont(titleFont);
	bottomText.setColor(sf::Color::White);
	bottomText.setPosition(50, 700);
	bottomText.setCharacterSize(120);

	instructionText.setFont(titleFont);
	instructionText.setColor(sf::Color::White);
	instructionText.setPosition(50, 1000);
	instructionText.setCharacterSize(100);

	background.setFillColor(sf::Color(50, 50, 50, 255));
	background.setPosition(0, 0);

	titleText.setString("Double Paddle");
	bottomText.setString("Game made by Maciej Kusnierz\nmaciek.hooch@gmail.com");
	instructionText.setString("Press SPACE to continue");

	isLoaded = true;
}

void StartScreenScene::Unload()
{
	isLoaded = false;
}

void StartScreenScene::Update(sf::Time currentTime, sf::Time dTime)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		auto playScene = new PlayingScene();
		SceneManager::GetInstance()->SetNextScene(playScene, true, true);
	}
}

void StartScreenScene::Render(sf::RenderWindow& renderWnd)
{
	//Constraining render to 4:3 windows size.
	sf::Vector2u wndSize = renderWnd.getSize();
	float whRatio = (float)wndSize.x / wndSize.y;
	float ratio = 1.0;
	if (whRatio < (4.0 / 3.0))
		ratio = wndSize.x / (wndSize.y * (4.0 / 3.0));

	float wholeSceneWidth = (wndSize.y * (4.0 / 3.0)) / (wndSize.x);
	projectionView.setViewport(sf::FloatRect(0, (1.0 - ratio) / 2, wholeSceneWidth * ratio, 1 * ratio));

	auto oldView = renderWnd.getView();
	renderWnd.setView(projectionView);

	renderWnd.draw(background);
	renderWnd.draw(titleText);
	renderWnd.draw(bottomText);
	renderWnd.draw(instructionText);


	//Restore
	renderWnd.setView(oldView);
}