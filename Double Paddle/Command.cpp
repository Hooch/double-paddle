#include "Command.h"

unsigned int BaseCommand::lastId = 0;

BaseCommand::BaseCommand()
{
	id = lastId++;
}

bool BaseCommand::operator==(const BaseCommand& other)
{
	return id == other.id;
}
