#include "BrickEntity.h"

BrickEntity::BrickEntity(int health, int maxHealth) : health(health), maxHealth(maxHealth)
{
	size.x = 0.30f;
	size.y = 0.10f;

	shape.setFillColor(sf::Color::White);
	shape.setSize(size);
	shape.setOrigin(size.x / 2, size.y / 2);
}

BrickEntity::BrickEntity(b2World& world) : BrickEntity()
{
	Register(world, { 0, 0 });
}

void BrickEntity::Register(b2World& world, const b2Vec2& initialPosition)
{
	b2BodyDef bodyDef = CreateBodyDefinition(initialPosition);
	body = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x/2, size.y/2);
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 5.0f;
	fixtureDef.restitution = 0.8f;
	fixtureDef.userData = this;

	fixture = body->CreateFixture(&fixtureDef);
}

void BrickEntity::SetHealth(int health)
{
	health = std::min(health, maxHealth);
}

int BrickEntity::GetHealth()
{
	return health;
}

void BrickEntity::SetMaxHealth(int maxHealth)
{
	maxHealth = maxHealth;
	SetHealth(health);
}

int BrickEntity::GetMaxHealth()
{
	return maxHealth;
}

b2BodyDef BrickEntity::CreateBodyDefinition(const b2Vec2& initialPosition)
{
	b2BodyDef bodyDefinition;
	bodyDefinition.type = b2_dynamicBody;
	bodyDefinition.linearDamping = BRICK_LINEAR_DUMPING;
	bodyDefinition.angularDamping = BRICK_ANGULAR_DUMPING;
	bodyDefinition.position = initialPosition;

	return bodyDefinition;
}

void BrickEntity::Draw(sf::RenderWindow& renderWnd)
{
	shape.setPosition(sfVectorFromB2Vector(body->GetPosition()));
	shape.setRotation((float)RAD2DEG(body->GetAngle()));

	renderWnd.draw(shape);
}

sf::Vector2f BrickEntity::GetSize()
{
	return size;
}

void BrickEntity::PostSolve(PhysicsEntity& other, b2Contact* contact, const b2ContactImpulse* impulse)
{
	PhysicsEntity::PostSolve(other, contact, impulse);
}

void BrickEntity::Update(const sf::Time& currentTime, const sf::Time& dTime)
{

}

void BrickEntity::EndContact(PhysicsEntity& other, b2Contact* contact)
{	
	if (other.GetEntityType() == ET_BALL)
	{
		health -= 50;
	}
	PhysicsEntity::EndContact(other, contact);
}
