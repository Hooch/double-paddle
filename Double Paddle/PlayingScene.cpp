#include "PlayingScene.h"

PlayingScene::PlayingScene() : world(b2Vec2(0, 0)),
ball(new BallEntity(0.0f)),
paddleBottom({ 1, 0 }, { PLAYING_WIDTH / 2, PLAYING_WIDTH - 0.1f }, PLAYING_WIDTH),
paddleTop({ -1, 0 }, { PLAYING_WIDTH / 2, 0.1f }, PLAYING_WIDTH),
paddleLeft({ 0, 1 }, { 0.1f, PLAYING_WIDTH / 2 }, PLAYING_WIDTH),
paddleRight({ 0, -1 }, { PLAYING_WIDTH - 0.1f, PLAYING_WIDTH / 2 }, PLAYING_WIDTH),
ballNotMoving(true), initialBallDone(false)
{
	timescale = 1.0;
	worldSize = { PLAYING_WIDTH, PLAYING_WIDTH };

	camera.setSize(worldSize);
	camera.setCenter(worldSize.x / 2, worldSize.y / 2);

	gameHud.SetScore(0);

	background.setFillColor(sf::Color::Blue);
	background.setPosition(0, 0);
	background.setSize(worldSize);

	world.SetContactListener(&collisionCollbacks);

	ball->CreateB2BallAndRegister({ PLAYING_WIDTH / 2, PLAYING_WIDTH - 0.5f }, { 0, -1 }, world);
	ballNotMoving = true;

	const float maxSpeed = 10.0f;
	paddleBottom.Register(world);
	paddleBottom.SetMaxSpeed(maxSpeed);
	paddleTop.Register(world);
	paddleTop.SetMaxSpeed(maxSpeed);
	paddleLeft.Register(world);
	paddleLeft.SetMaxSpeed(maxSpeed);
	paddleRight.Register(world);
	paddleRight.SetMaxSpeed(maxSpeed);

	score = 0;
	ballsLost = 0;
	bricksKilledInRow = 0;

	paused = false;

	isLoaded = false;
}

PlayingScene::~PlayingScene()
{
	delete ball;
}

void PlayingScene::Render(sf::RenderWindow& renderWnd)
{
	sf::Vector2u wndSize = renderWnd.getSize();
	float whRatio = (float)wndSize.x / wndSize.y;
	float ratio = 1.0;
	if (whRatio < (4.0 / 3.0))
	{
		ratio = wndSize.x / (wndSize.y * (4.0 / 3.0));
	}

	float wholeGameWidth = (wndSize.y * (4.0 / 3.0)) / (wndSize.x);
	camera.setViewport(sf::FloatRect(0, (1.0 - ratio) / 2, wholeGameWidth * (3.0 / 4.0) * ratio, 1 * ratio));
	gameHud.SetViewport(sf::FloatRect(wholeGameWidth * (3.0 / 4.0) * ratio, (1.0 - ratio) / 2, wholeGameWidth * (1.0 / 4.0) * ratio, 1 * ratio));

	renderWnd.setView(camera);

	renderWnd.draw(background);

	for (auto& brick : Bricks)
	{
		brick->Draw(renderWnd);
	}

	ball->Draw(renderWnd);
	paddleBottom.Draw(renderWnd);
	paddleTop.Draw(renderWnd);
	paddleLeft.Draw(renderWnd);
	paddleRight.Draw(renderWnd);

	gameHud.Draw(renderWnd);

	if (paused)
	{
		renderWnd.draw(pauseSprite);
	}

	renderWnd.setView(renderWnd.getDefaultView());
}

void PlayingScene::Update(sf::Time currentTime, sf::Time dTime)
{
	HandleInput(currentTime, dTime);
	if (paused) return;

	ball->Update(currentTime, dTime);
	paddleBottom.Update(currentTime, dTime);
	paddleTop.Update(currentTime, dTime);
	paddleLeft.Update(currentTime, dTime);
	paddleRight.Update(currentTime, dTime);

	world.Step(dTime.asSeconds() * timescale, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
	ManageBalls(currentTime, dTime);

	for (auto& brick : Bricks)
	{
		brick->Update(currentTime, dTime);
	}

	Bricks.remove_if([](const std::unique_ptr<BrickEntity> &brick) {return brick->GetHealth() <= 0; });

	gameHud.Update(currentTime, dTime);

	//End da game
	if (Bricks.size() == 0)
	{
		auto endScene = new EndGameScene();
		endScene->SetValues(score, ballsLost);
		SceneManager::GetInstance()->SetNextScene(endScene, true, true);
	}
}

void PlayingScene::Unload()
{
	isLoaded = false;
}

void PlayingScene::Load()
{
	CreateBricks();

	//Paused text rendering
	pauseTexture.create(8000, 8000);
	sf::Font font;
	font.loadFromFile("Assets\\Fonts\\JLSSpaceGothicR_NC.otf");
	sf::Text pausedText;
	pausedText.setString("Game paused");
	pausedText.setFont(font);
	pausedText.setCharacterSize(1000);
	pausedText.setColor(sf::Color::White);
	pausedText.setPosition(300, 2200);
	auto textBounds = pausedText.getGlobalBounds();
	sf::RectangleShape bg;
	bg.setSize({ textBounds.width + 5000, textBounds.height + 800 });
	bg.setPosition(0, textBounds.top - 400);	
	bg.setFillColor(sf::Color(50, 50, 50, 255));

	pauseTexture.draw(bg);
	pauseTexture.draw(pausedText);
	pauseTexture.setSmooth(true);
	pauseTexture.display();
	pauseSprite.setTexture(pauseTexture.getTexture());
	pauseSprite.setScale(0.001, 0.001);

	isLoaded = true;
}

void PlayingScene::CreateBricks()
{
	BrickEntity* brick = new BrickEntity();

	const int columns = 5;
	const int rows = 9;
	const float columnSpace = 0.1f;
	const float rowSpace = 0.1f;

	float totalBricksWidth = (columns * brick->GetSize().x) + ((columns - 1) * columnSpace);
	float totalBricksHeight = (rows * brick->GetSize().y) + ((rows - 1) * rowSpace);

	b2Vec2 offsetToCenter = { ((worldSize.x - totalBricksWidth) / 2) + (brick->GetSize().x / 2), ((worldSize.y - totalBricksHeight) / 2) + (brick->GetSize().y / 2) };

	delete brick; //Was used only to get size

	for (int r = 0; r < rows; r++)
	{
		for (int c = 0; c < columns; c++)
		{
			auto brick = std::unique_ptr<BrickEntity>(new BrickEntity());
			b2Vec2 brickPos = { c * (brick->GetSize().x + columnSpace), r * (brick->GetSize().y + rowSpace) };
			brickPos += offsetToCenter;
			brick->Register(world, brickPos);
			brick->AddEndContactCallback(EndContactCallback(std::bind(&PlayingScene::HandleBrickHit, this, std::placeholders::_2, std::placeholders::_3)));
			Bricks.push_back(std::move(brick));
		}
	}
}

void PlayingScene::HandleInput(const sf::Time& currentTime, const sf::Time& dTime)
{
	if (!paused)
	{
		//Top-bottom paddle
		if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && sf::Keyboard::isKeyPressed(sf::Keyboard::Left)))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				paddleBottom.MoveRight(dTime);
				paddleTop.MoveLeft(dTime);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				paddleBottom.MoveLeft(dTime);
				paddleTop.MoveRight(dTime);
			}
		}

		//Left-Right paddle
		if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && sf::Keyboard::isKeyPressed(sf::Keyboard::Up)))
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				paddleLeft.MoveRight(dTime);
				paddleRight.MoveLeft(dTime);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				paddleLeft.MoveLeft(dTime);
				paddleRight.MoveRight(dTime);
			}
		}
	}

	static bool wasPauseBtnUp = true;
	static bool wasReleasedSinceStart = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) ||
		sf::Keyboard::isKeyPressed(sf::Keyboard::Space) ||
		sf::Keyboard::isKeyPressed(sf::Keyboard::Pause))
	{		
			if (wasPauseBtnUp && wasReleasedSinceStart)
			{
				paused = !paused;
				wasPauseBtnUp = false;
			}
	}
	else
	{
		wasReleasedSinceStart = true;
		wasPauseBtnUp = true;
	}
}

BallEntity* PlayingScene::SpawnBall(const b2Vec2& pos, const b2Vec2& dir, float speed)
{
	BallEntity* tmpBall = new BallEntity(speed);
	tmpBall->CreateB2BallAndRegister(pos, dir, world);
	return tmpBall;
}

void PlayingScene::ManageBalls(sf::Time currentTime, sf::Time dTime)
{
	auto ballPos = ball->GetPosition();

	if (!initialBallDone)
	{
		delete ball;
		ball = SpawnBall({ PLAYING_WIDTH / 2, PLAYING_WIDTH - 0.5f }, { 0, -1 }, 0.0f);
		ballSpawnTime = currentTime;
		initialBallDone = true;
		ballNotMoving = true;
		return;
	}

	if (ballPos.x > PLAYING_WIDTH || ballPos.x < 0 || ballPos.y > PLAYING_WIDTH || ballPos.y < 0)
	{
		delete ball;
		ball = SpawnBall({ PLAYING_WIDTH / 2, PLAYING_WIDTH - 0.5f }, { 0, -1 }, 0.0f);
		ballSpawnTime = currentTime;
		ballNotMoving = true;
		gameHud.SetBallsUsed(++ballsLost);
		gameHud.SetScore(score -= 500);
		bricksKilledInRow = 0;
	}

	if (ballNotMoving && ((currentTime - ballSpawnTime).asSeconds() > 2.0f))
	{
		ball->SetTargetSpeed(BALL_SPEED);
		ballNotMoving = false;
	}
}

void PlayingScene::HandleBrickHit(PhysicsEntity* pbrick, PhysicsEntity* other)
{
	if (pbrick->GetEntityType() != ET_BRICK) return;
	BrickEntity* brick = (BrickEntity*)pbrick;

	if (other->GetEntityType() == ET_BALL)
	{
		if (brick->GetHealth() <= 0)
		{
			bricksKilledInRow++;
			gameHud.SetScore(score += (100 + (20 * bricksKilledInRow)));
		}
		else
		{
			gameHud.SetScore(score += 100);
		}
	}
}
