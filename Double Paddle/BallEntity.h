#pragma once

#include "PhysicsEntity.h"
#include "DrawableEntity.h"
#include "GlobalDefines.h"
#include <algorithm>
#include <SFML/Graphics.hpp>
#include <iostream>

class BallEntity : public PhysicsEntity, public DrawableEntity
{
public:
	BallEntity(float targetSpeed);

	virtual EntityType GetEntityType();
	virtual void Update(const sf::Time& currentTime, const sf::Time& dTime);
	virtual void Draw(sf::RenderWindow& renderWnd);

	void SetTargetSpeed(float speed);
	float GetTargetSpeed();
	void SetSize(float size);
	float GetSize();

	void CreateB2BallAndRegister(const b2Vec2& pos, const b2Vec2& dir, b2World& world);

	virtual void PostSolve(PhysicsEntity& other, b2Contact* contact, const b2ContactImpulse* impulse);

private:
	b2Vec2 initialMovDir;
	float targetSpeed;
	float size;
	sf::CircleShape shape;
};