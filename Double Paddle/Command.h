#pragma once
#include <memory>
#include <list>
#include <functional>

class BaseCommand
{
public:
	BaseCommand();
	virtual ~BaseCommand() {}
	bool operator==(const BaseCommand& other);

protected:
	unsigned int id;
	static unsigned int lastId;
};

template <class... ArgTypes>
class Command : public BaseCommand
{
	typedef std::function<void(ArgTypes...)> FuncType;
	FuncType f;
public:
	Command() {}
	Command(FuncType func) : f(func) {}
	void operator()(ArgTypes... args) { if (f) f(args...); }
};