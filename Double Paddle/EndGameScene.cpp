#include "EndGameScene.h"

EndGameScene::EndGameScene() :
score(0), ballsLost(0), bestScore(0), 
projectionView({ 0, 0, 1600, 1200 }), background(projectionView.getSize())
{
	std::ifstream inScoreFile("highscore.save");
	if (inScoreFile.is_open())
	{
		inScoreFile >> bestScore;
		bestScore += 10000;
		bestScore ^= SCORE_KEY;

		inScoreFile.close();
	}

	isLoaded = false;
}

EndGameScene::~EndGameScene()
{

}

void EndGameScene::Load()
{
	scoreFont.loadFromFile("Assets\\Fonts\\JLSSpaceGothicR_NC.otf");
	const int FONT_SIZE = 100;
	const int NEXT_LINE = FONT_SIZE + 10;
	int currentLine = 0;

	scoreText.setFont(scoreFont);
	scoreText.setColor(sf::Color::White);
	scoreText.setCharacterSize(FONT_SIZE);
	scoreText.setPosition(50, currentLine);

	ballsUsedText.setFont(scoreFont);
	ballsUsedText.setColor(sf::Color::White);
	ballsUsedText.setPosition(50, currentLine += NEXT_LINE * 2);
	ballsUsedText.setCharacterSize(FONT_SIZE);

	bestScoreText.setFont(scoreFont);
	bestScoreText.setColor(sf::Color::White);
	bestScoreText.setPosition(50, currentLine += NEXT_LINE * 4);
	bestScoreText.setCharacterSize(FONT_SIZE);

	bottomText.setFont(scoreFont);
	bottomText.setColor(sf::Color::White);
	bottomText.setPosition(50, currentLine += NEXT_LINE * 3);
	bottomText.setCharacterSize(FONT_SIZE);

	background.setFillColor(sf::Color(50, 50, 50, 255));
	background.setPosition(0, 0);

	isLoaded = true;
}

void EndGameScene::Unload()
{
	isLoaded = false;
}

void EndGameScene::Update(sf::Time currentTime, sf::Time dTime)
{
	std::ostringstream oss;
	oss << "Final Score:\n" << score;
	scoreText.setString(oss.str());

	oss.str(std::string());
	oss << "Balls lost:\n" << ballsLost;
	ballsUsedText.setString(oss.str());

	oss.str(std::string());
	if (score > bestScore)
		oss << "Previous best score:\n";
	else
		oss << "Best score:\n";
	oss << bestScore;
	bestScoreText.setString(oss.str());

	bottomText.setString("Press SPACE to try again");

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		auto playScene = new PlayingScene();
		SceneManager::GetInstance()->SetNextScene(playScene, true, true);
	}
}

void EndGameScene::Render(sf::RenderWindow& renderWnd)
{
	//Constraining render to 4:3 windows size.
	sf::Vector2u wndSize = renderWnd.getSize();
	float whRatio = (float)wndSize.x / wndSize.y;
	float ratio = 1.0;
	if (whRatio < (4.0 / 3.0))
		ratio = wndSize.x / (wndSize.y * (4.0 / 3.0));

	float wholeSceneWidth = (wndSize.y * (4.0 / 3.0)) / (wndSize.x);
	projectionView.setViewport(sf::FloatRect(0, (1.0 - ratio) / 2, wholeSceneWidth * ratio, 1 * ratio));


	auto oldView = renderWnd.getView();
	renderWnd.setView(projectionView);

	renderWnd.draw(background);
	renderWnd.draw(scoreText);
	renderWnd.draw(ballsUsedText);
	renderWnd.draw(bestScoreText);
	renderWnd.draw(bottomText);

	//Restore
	renderWnd.setView(oldView);
}

void EndGameScene::SetValues(int score, int ballsLost)
{
	this->score = score;
	this->ballsLost = ballsLost;

	if (score > bestScore)
	{
		std::ofstream oScoreFile("highscore.save", std::ios::trunc);
		if (oScoreFile.is_open())
		{	
			int temp = score;			
			temp ^= SCORE_KEY;
			temp -= 10000;
			oScoreFile << temp;

			oScoreFile.close();
		}
	}
}

