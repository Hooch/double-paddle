#pragma once
#include "PhysicsEntity.h"
#include "GlobalDefines.h"

#include "DrawableEntity.h"
#include <algorithm>

#include <SFML/Graphics.hpp>
#include <iostream>

class PaddleEntity : public PhysicsEntity, public DrawableEntity
{
public:
	PaddleEntity(const b2Vec2& direction, const b2Vec2& centerPos, float totalMovement);

	virtual EntityType GetEntityType() { return EntityType::ET_PADDLE; }
	virtual void Draw(sf::RenderWindow& renderWnd);
	virtual void Update(const sf::Time& currentTime, const sf::Time& dTime);

	void Register(b2World& world);

	void MoveLeft(const sf::Time& dTime);
	void MoveRight(const sf::Time& dTime);

	void SetMaxSpeed(float maxSpeed);
	
private:
	b2BodyDef CreateBodyDefinition(const b2Vec2& initialPosition);
	void RestrictMovement();

	b2Vec2 direction;
	b2Vec2 centerPos;
	float totalMovement;
	float maxSpeed;
	sf::Vector2f size;
	sf::RectangleShape shape;

	bool acceptRightInput;
	bool acceptLeftInput;

	const float moveForce = 750;
};