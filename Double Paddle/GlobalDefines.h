#pragma once

//All releases
#define VELOCITY_ITERATIONS 8
#define POSITION_ITERATIONS	3

#define PLAYING_WIDTH 6.0f
#define PADDLE_RANDOM_BOUNCE 0.05f
#define SCORE_KEY 0x58F8C4EA

#define BRICK_LINEAR_DUMPING 3
#define BRICK_ANGULAR_DUMPING 5
#define BALL_DENSITY 4.0f

//Debug defines
#ifdef _DEBUG

#define WINDOW_NAME "Double Paddle - Debug build"
#define PADDLE_WIDTH 4.0f
#define BALL_SPEED	4.0f

#else // Release

#define WINDOW_NAME "Double Paddle"
#define PADDLE_WIDTH 1.0f
#define BALL_SPEED	4.0f

#endif // If not _DEBUG#