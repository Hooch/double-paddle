#pragma once

enum EntityType
{
	ET_ENTITY,
	ET_PHYSICS,
	ET_BRICK,
	ET_BALL,
	ET_PADDLE
};

class Entity
{
public:
	Entity() = default;
	virtual ~Entity() = default;
	
	virtual EntityType GetEntityType()
	{
		return ET_ENTITY;
	}
};