#pragma once
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"

class Scene
{
public:
	Scene();
	virtual ~Scene();

	virtual void Load();
	virtual void Unload();
	virtual bool IsLoaded();

	virtual void Update(sf::Time currentTime, sf::Time dTime) = 0;
	virtual void Render(sf::RenderWindow& renderWnd) = 0;

protected:
	bool isLoaded;	
};